// Función para animar el título de la página con efecto de fade in
function animateTitle() {
    var pageTitle = document.getElementById('pageTitle');
    pageTitle.style.opacity = 0; // Establece la opacidad inicial del título a 0

    var fadeInInterval = setInterval(function() {
        pageTitle.style.opacity = parseFloat(pageTitle.style.opacity) + 0.05; // Incrementa la opacidad gradualmente
        if (pageTitle.style.opacity >= 1) { // Verifica si se alcanzó la opacidad completa
            clearInterval(fadeInInterval); // Detiene la animación
        }
    }, 50); // Intervalo de tiempo entre cada incremento de opacidad
}

// Llama a la función cuando se carga completamente el documento
document.addEventListener('DOMContentLoaded', function() {
    animateTitle(); // Inicia la animación del título
});
