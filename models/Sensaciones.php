<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sensaciones".
 *
 * @property int $codsensa
 * @property string|null $descripcion
 * @property string|null $denominacion
 * @property string|null $localizacioncorporal
 *
 * @property Entradas[] $identradas
 * @property Registrosensaciones[] $registrosensaciones
 */
class Sensaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sensaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'denominacion', 'localizacioncorporal'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codsensa' => 'Codsensa',
            'descripcion' => 'Descripcion',
            'denominacion' => 'Denominacion',
            'localizacioncorporal' => 'Localizacioncorporal',
        ];
    }

    /**
     * Gets query for [[Identradas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdentradas()
    {
        return $this->hasMany(Entradas::class, ['identrada' => 'identrada'])->viaTable('registrosensaciones', ['codsensa' => 'codsensa']);
    }

    /**
     * Gets query for [[Registrosensaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegistrosensaciones()
    {
        return $this->hasMany(Registrosensaciones::class, ['codsensa' => 'codsensa']);
    }
    
   
}
