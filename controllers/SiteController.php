<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Entradas;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
 public function actionIndex()
{
    try {
        //obtengo las entradas de diario más recientes
        $entradas = \app\models\Entradas::find()->orderBy(['fechaentrada' => SORT_DESC])->limit(6)->all();
    } catch (\Exception $e) {
        Yii::error('Error al recuperar las entradas de diario: ' . $e->getMessage());
        $entradas = [];
    }

    //llamo a la acción para contar las entradas positivas y negativas
    $countResult = $this->actionGraficoEntradas();

    return $this->render('index', [   //renderizo en el index 
        'entradas' => $entradas,
        'positivas' => $countResult['positivas'],
        'negativas' => $countResult['negativas'],
    ]);
}

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
   /*mi primera explotacion de datos con los datos detallados de las ultimas 6 entradas del diario*/
public function actionView($id)
{
    // Buscar la entrada por su ID
    $entrada = Entradas::findOne($id);

    // Verificar si la entrada existe
    if (!$entrada) {
        throw new \yii\web\NotFoundHttpException("La entrada con el ID $id no existe.");
    }

    //Cargar los pensamientos asociados a esta entrada
    $pensamientos = $entrada->getCodpens()->all();
    //cargar las emociones asociadas a esta entrada
    $emociones = $entrada->getCodemos()->all();

    $sensaciones = $entrada->getCodsensas()->all();

    return $this->render('vista_detalles', [
    'entrada' => $entrada,
    'pensamientos' => $pensamientos,
    'sensaciones' => $sensaciones,
    'emociones' => $emociones, 
]);

}

/*grafico*/


public function actionGraficoEntradas()
{
    try {
        // Obtener las entradas de diario más recientes
        $entradas = \app\models\Entradas::find()->orderBy(['fechaentrada' => SORT_DESC])->limit(6)->all();

        //cont para las entradas positivas y negativas
        $positivas = 0;
        $negativas = 0;
        //contar la cantidad de entradas positivas y negativas
        foreach ($entradas as $entrada) {
            $pensamientos = $entrada->getCodpens()->all();
            foreach ($pensamientos as $pensamiento) {
                if ($pensamiento->positivo == 1) {
                    $positivas++;
                } else {
                    $negativas++;
                }
            }
        }
    } catch (\Exception $e) {
        Yii::error('Error al contar las entradas de diario: ' . $e->getMessage());
        $positivas = 0;
        $negativas = 0;
    }

    //retorno un arreglo asociativo con los conteos de entradas positivas y negativas 
    return [
        'positivas' => $positivas,
        'negativas' => $negativas,
    ];
}


public function actionInformacion(){
    
     return $this->render('informacion');
    
}






    
}
