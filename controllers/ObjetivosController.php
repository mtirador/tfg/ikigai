<?php

namespace app\controllers;

use Yii; // Agrega esta línea para importar la clase Yii
use app\models\Objetivos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

// Resto del código del controlador...

/**
 * ObjetivosController implements the CRUD actions for Objetivos model.
 */
class ObjetivosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Objetivos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Objetivos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codobj' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Objetivos model.
     * @param int $codobj Codobj
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codobj)
    {
        return $this->render('view', [
            'model' => $this->findModel($codobj),
        ]);
    }

    /**
     * Creates a new Objetivos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
   public function actionCreate()
    {
        $model = new Objetivos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codobj' => $model->codobj]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    // Resto del código del controlador...


    /**
     * Updates an existing Objetivos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codobj Codobj
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    
    public function actionUpdate($codobj)
    {
        $model = $this->findModel($codobj);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codobj' => $model->codobj]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
/**
 * Deletes an existing Objetivos model.
 * If deletion is successful, the browser will be redirected to the 'index' page.
 * @param int $codobj Codobj
 * @return \yii\web\Response
 * @throws NotFoundHttpException if the model cannot be found
 */
public function actionDelete($codobj)
{
    // Encuentra el modelo del objetivo a eliminar
    $model = $this->findModel($codobj);
    // Elimina el modelo de la base de datos
    $model->delete();

    // Obtener el último codobj existente en la base de datos
    $lastCodobj = Objetivos::find()->max('codobj');

    // Actualizar el contador codobj
    // resetSequence() reinicia la secuencia de la tabla a un nuevo valor especificado
    // 'objetivos' es el nombre de la tabla
    // $lastCodobj + 1 es el nuevo valor para la secuencia
    Yii::$app->db->createCommand()->resetSequence('objetivos', $lastCodobj + 1)->execute();

    // Redirige a la página de índice después de la eliminación
    return $this->redirect(['index']);
}



    /**
     * Finds the Objetivos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codobj Codobj
     * @return Objetivos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codobj)
    {
        if (($model = Objetivos::findOne(['codobj' => $codobj])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    
    //Función para la creación del formulario
    public function actionCreateCustom(){
    $model = new Objetivos();

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
        // Si se guardó correctamente, redirige a la página de detalle del nuevo objetivo
        return $this->redirect(['view', 'codobj' => $model->codobj]);
    } else {
        // Si hubo algún error al guardar, vuelve a mostrar el formulario
        return $this->render('create_custom', [
            'model' => $model,
        ]);
    }
}

    
    
    
    
    
    
    
}
