<?php

namespace app\controllers;

use app\models\Emociones;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EmocionesController implements the CRUD actions for Emociones model.
 */
class EmocionesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Emociones models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Emociones::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codemo' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Emociones model.
     * @param int $codemo Codemo
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codemo)
    {
        return $this->render('view', [
            'model' => $this->findModel($codemo),
        ]);
    }

    /**
     * Creates a new Emociones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Emociones();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codemo' => $model->codemo]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Emociones model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codemo Codemo
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codemo)
    {
        $model = $this->findModel($codemo);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codemo' => $model->codemo]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Emociones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codemo Codemo
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codemo)
    {
        $this->findModel($codemo)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Emociones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codemo Codemo
     * @return Emociones the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codemo)
    {
        if (($model = Emociones::findOne(['codemo' => $codemo])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
