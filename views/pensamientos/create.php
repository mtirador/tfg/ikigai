<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Pensamientos $model */

$this->title = 'Create Pensamientos';
$this->params['breadcrumbs'][] = ['label' => 'Pensamientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pensamientos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
