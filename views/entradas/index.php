<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Entradas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entradas-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= Html::a('Crear Entrada', ['create'], ['class' => 'btn btn-success']) ?></p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'identrada',
            'titulo',
            'descripcion:ntext',
            'fechaentrada',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
