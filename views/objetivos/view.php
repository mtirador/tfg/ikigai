<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Objetivos */

$this->registerCssFile(Url::to('@web/css/formulario.css'));
$this->title = $model->denominacion;
$this->params['breadcrumbs'][] = ['label' => 'Objetivos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<style>
    /* Estilos Generales */
    h1 {
        color: #B65598;
    }
    
    .alert.alert-info {
        background-color: transparent;
        color: #B65598;
        border-color: transparent;
    }
    
    .panel.panel-default {
        background-color: transparent;
        color: #555;
    }
    
    .panel-heading {
        background-color: #e6d7f6;
        color: #B65598;
    }
    
    .panel-title {
        color: #b65598;
    }
    
    .panel-body {
        color: #b65598;
    }

    /* Botones */
    .btn-primary {
        background-color: #e6d7f6;
        border-color: #e6d7f6;
    }
    
    .btn-primary:hover {
        background-color: #e6d7f6;
        border-color: #e6d7f6;
    }
    
    .btn-danger {
        background-color: #e6d7f6;
        border-color: #e6d7f6;
    }
    
    .btn-group > .btn:not(:first-child),
    .btn-group > .btn-group:not(:first-child) {
        margin-left: 10px;
    }
    
    .btn-danger:hover {
        background-color: #e6d7f6;
        border-color: #e6d7f6;
    }

    /* Estilos del Contenedor Principal */
    .objetivos-view {
        margin-top: 50px;
    }

    /* Estilos de las Columnas */
    .col-md-6 {
        margin-bottom: 20px;
    }

    /* Ajustar la altura de las columnas */
    .row-eq-height {
        display: flex;
        flex-wrap: wrap;
    }

    .row-eq-height > [class*='col-'] {
        display: flex;
        flex-direction: column;
    }

    /* Enlaces */
    a {
        color: #B65598 !important;
        text-decoration: none;
        background-color: transparent;
    }

    a:hover {
        color: #d60c9f !important;
        text-decoration: underline;
    }

    /* Estilo para tabla detail-view */
    .table.detail-view {
        border-collapse: collapse;
        width: 100%;
        margin-bottom: 20px;
    }

    /* Estilo para cabecera de tabla */
    .table.detail-view th {
        background-color: #e6d7f6;
        color: #B65598;
        font-weight: bold;
        border: 1px solid #dee2e6;
        padding: 8px;
    }

    /* Estilo para celdas de tabla */
    .table.detail-view td {
        border: 1px solid #dee2e6;
        padding: 8px;
    }

    /* Estilo para filas impares */
    .table.detail-view tr:nth-child(odd) {
        background-color: #f8f9fa;
    }

    /* Estilo para filas pares */
    .table.detail-view tr:nth-child(even) {
        background-color: #ffffff;
    }

    /* Estilo para enlaces dentro de la tabla */
    .table.detail-view a {
        color: #B65598;
        text-decoration: none;
    }

    /* Estilo para enlaces al pasar el ratón */
    .table.detail-view a:hover {
        text-decoration: underline;
    }

    /* Estilo para celdas de tabla */
    .table.detail-view td {
        border-color: transparent;
        padding: 10px;
        color: #b65598;
        background-color: #e6d7f6;
    }
</style>

<div class="objetivos-view">
    <div class="container">
        <div class="row row-eq-height">
            <div class="col-md-6">
                <!-- Mensaje de Confirmación -->
                <div class="alert alert-info" role="alert">
                    ¡Objetivo Guardado!
                </div>

                <!-- Detalles del Objetivo -->
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'denominacion',
                        'fechalimite',
                        'descripcion',
                    ],
                ]) ?>

                <!-- Acciones Adicionales -->
                <div class="btn-group" role="group" aria-label="Acciones adicionales">
                    <?= Html::a('Editar', ['update', 'codobj' => $model->codobj], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Eliminar', ['delete', 'codobj' => $model->codobj], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => '¿Estás seguro de que deseas eliminar este objetivo?',
                            'method' => 'post',
                        ],
                    ]) ?>
                    <!-- Agrega aquí más botones de acción adicionales si es necesario -->
                </div>
            </div>

            <div class="col-md-6">
                <!-- Instrucciones o Sugerencias -->
                <div class="panel panel-default mt-5">
                    <div class="panel-heading">
                        <h3 class="panel-title">Sugerencias</h3>
                    </div>
                    <div class="panel-body">
                        <p>¡Sigue adelante y haz realidad este objetivo! Recuerda mantenerte enfocado y dedicar tiempo regularmente para trabajar en él.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
