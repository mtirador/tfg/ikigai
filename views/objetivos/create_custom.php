<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Objetivos */
/* @var $form yii\widgets\ActiveForm */

$this->registerCssFile(Url::to('@web/css/formulario.css'));

$this->title = 'Crear Nuevo Objetivo';
$this->params['breadcrumbs'][] = ['label' => 'Objetivos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="objetivos-create">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'denominacion')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'Ingrese el título de la entrada aquí']) ?>

                    <?= $form->field($model, 'fechalimite')->textInput(['type' => 'date', 'class' => 'form-control', 'placeholder' => 'Seleccione la fecha límite', 'min' => date('Y-m-d')])
                        ->hint('Elija la fecha en la que desea completar este objetivo. Esto ayudará a mantenerse enfocado en su progreso.');
                    ?>

                    <?= $form->field($model, 'descripcion')->textarea([
                        'rows' => 5, 
                        'class' => 'form-control', 
                        'placeholder' => 'Ingrese la descripción aquí', 
                        'maxlength' => 100 //Establece la longitud máxima del campo a 100 caracteres
                     ])->label('Descripción') ?>


                    <div class="form-group">
                        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <img src="<?= Url::to('@web/images/chill2.png') ?>" alt="Chica con flores" class="imagen-formulario">
        </div>
    </div>
</div>

<?php
$this->registerJs("
    $(document).on('submit', '#{$form->id}', function(event) {
        if (!confirm('¿Estás seguro de que deseas guardar este objetivo?')) {
            event.preventDefault();
        }
    });
");
       
?>
