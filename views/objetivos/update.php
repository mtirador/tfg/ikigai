<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Objetivos $model */

$this->title = 'Editar Objetivo: ' . $model->denominacion;
$this->params['breadcrumbs'][] = ['label' => 'Objetivos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codobj, 'url' => ['view', 'codobj' => $model->codobj]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="objetivos-update">

    <div class="row">
        <div class="col-md-12">
            <h1 class="text-justify"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

</div>
