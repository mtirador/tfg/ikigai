<?php
/** @var yii\web\View $this */
$this->title = 'My Yii Application';
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View; 

$this->registerCssFile(Url::to('@web/css/entradasDiario.css'));


$this->registerJsFile('@web/js/titulo.js', ['depends' => [\yii\web\JqueryAsset::class]]);



$this->registerJsFile(Url::to('@web/js/scrollVisibilidad.js'), ['position' => View::POS_END]);
?>



<body>
    
<div class="tituloHome">
    <div class="background-image">
    <h1 id="pageTitle" class="animated-title">IKIGAI</h1>
    <hr class="linea-larga">
    <p class="subtitulo">"Tu propósito de vida"</p>
    </div>
</div>
    
    
<div class="contenedor">
    <div class="tarjeta-boton entry">
        <a href="<?= \yii\helpers\Url::to(['entradas/create']) ?>">
            <div class="card-inner">
                <img src="./images/d2.png" alt="Imagen 1">
                <div class="overlay">
                    <h3>Crear Entrada</h3>
                </div>
            </div>
        </a>
    </div>
    <div class="tarjeta-boton entry">
        <a href="<?= \yii\helpers\Url::to(['objetivos/create-custom']) ?>"> <!-- Cambiado a 'create-custom' -->
            <div class="card-inner">
                <img src="./images/obje.png" alt="Imagen 2">
                <div class="overlay">
                    <h3>Establecer Objetivo</h3>
                </div>
            </div>
        </a>
    </div>
</div>



<!--    imagen de fondo 
    <div class="custom-container">
       
    </div>

  
        <div class="texto-infor">          
            <h1>Descubre más sobre Ikigai</h1>
        </div>
  
   
 <div class="container">
    <div class="row d-flex">
         Tarjeta 1 
        <div class="col-md-4 mb-4">
            <div class="card-container" id="ikigai-card">
                <div class="image-container">
                    <img src="./images/pequeIki.png" class="imagen-transparente" alt="Descripción de la imagen">
                </div>
                <div class="content-container">
                    <h2>Significado de Ikigai</h2>
                    <p>Según la cultura japonesa todos tenemos un ikigai, es decir, un propósito de vida o una actividad que nos hace muy felices. Encontrarlo requiere emprender un camino de emprendimiento y crecimiento personal, es decir, un autoconocimiento constante y tenaz que lleva al mejor de los premios: calidad de vida.</p>
                </div>
            </div>
        </div>

         Tarjeta 2 
        <div class="col-md-4 mb-4">
            <div class="card-container" id="Funcionalidad-card">
                <div class="image-container">
                    <img src="./images/peqFun.png"class="imagen-transparente" alt="Descripción de la imagen">
                </div>
                <div class="content-container">
                                <h2>¿Para qué sirve nuestra aplicación?</h2>
                         
                                <strong><p>Comprender tus Emociones:</strong> Identifica tus emociones para un mayor autoconocimiento.</p>
                                     <strong><p>Gestionar el Estrés:</strong> Encuentra técnicas y consejos para manejar el estrés diario.</p>
                                     <strong><p>Descubrir tu Ikigai:</strong> Explora actividades que te apasionen y contribuyan a tu bienestar general.</p>
                                                               
                </div>
            </div>
        </div>

         Tarjeta 3 
        <div class="col-md-4 mb-4">
            <div class="card-container" id="Objetivos-card">
                <div class="image-container">
                    <img src="./images/peqObj.png" class="imagen-transparente" alt="Descripción de la imagen">
                </div>
                <div class="content-container">
                              <h2>Objetivos Personales</h2>
            
                              <p>Establecer metas es fundamental para visualizar lo que verdaderamente es importante en tu vida. Te proporciona una dirección clara y te motiva a alcanzar tu máximo potencial. Requiere autodisciplina para superar los obstáculos que inevitablemente surgen en el camino. Al lograr tus metas, experimentas un profundo sentido de logro y satisfacción personal, lo cual fortalece tu autoestima y confianza en ti mismo.</p>
                  </div>                  
            </div>
        </div>
    </div>
</div>
   --> 
   
   
   <!-- Entradas diario -->

<div class="site-index">
<div class="jumbotron entry">
        <h2 class="titulo-entradas">Últimas Entradas de Diario</h2> 
</div>

     <style>
        /*Oculta las entradas al principio */
        .entry {
            opacity: 0;
            transition: opacity 0.5s ease-in-out;
        }

        /* Aparecen gradualmente las entradas cuando están en el área visible del navegador */
        .entry.visible {
            opacity: 1;
        }
    </style>
    <div class="body-content">
       
        <div class="row"> 
        
            <?php foreach ($entradas as $entrada): ?>
                <div class="col-lg-4 entry">
                    <div class="card-container mb-3">
                        <div class="card">
                            <div class="card-header">
                                <h3><?= Html::encode($entrada->titulo) ?></h3>
                            </div>
                            <div class="card-body">
                                <p><strong>Fecha :</strong> <?= Yii::$app->formatter->asDate($entrada->fechaentrada) ?></p> 
                                <p><?= Html::encode($entrada->descripcion) ?></p>
                                <?= Html::a('Ver detalles', ['site/view', 'id' => $entrada->identrada], ['class' => 'btn btn-primary']) ?>

                                
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

    </div>
</div>

<div id="grafica-container" class="grafica-container entry">
    <?php
    use dosamigos\highcharts\HighCharts; // para utilizar la extensión de los gráficos

    //datos de la gráfica
    $data = [
        ['name' => 'Pensamientos Positivos', 'y' => $positivas],
        ['name' => 'Pensamientos Negativos', 'y' => $negativas],
    ];
    echo HighCharts::widget([
        'clientOptions' => [
            'chart' => [
                'type' => 'pie', // tipo de gráfico de pastel
                'backgroundColor' => 'rgba(0, 0, 0, 0)', //Fondo transparente
            ],
            'title' => ['text' => null], //Sin título
            'plotOptions' => [
                'pie' => [
                    'dataLabels' => [
                        'enabled' => true,
                        'style' => [
                            'color' => '#B65598', //Color del texto
                            'fontFamily' => '"Open Sans", Roboto, Lato, Montserrat, Nunito, "Source Sans Pro", "PT Sans", sans-serif',
                        ],
                    ],
                ],
            ],
            'series' => [
                [
                    'name' => 'Cantidad',
                    'data' => $data,
                    'dataLabels' => [
                        'enabled' => true,
                        'format' => '<b>{point.name}</b>: {point.percentage:.1f} %',
                    ],
                    'colors' => ['#B65598', '#e6d7f6'], //Colores de los trozos de pastel (positivo y negativo)
                ],
            ],
            'credits' => ['enabled' => false], //Desactivar los créditos de HighCharts
        ],
    ]);
    ?>
</div>

</body>



