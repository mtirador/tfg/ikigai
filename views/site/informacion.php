
<?php
/** @var yii\web\View $this */
$this->title = 'My Yii Application';
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="custom-container">
       
    </div>

  
        <div class="texto-infor">          
            <h1>Descubre más sobre Ikigai</h1>
        </div>
  

<div class="row d-flex">
    <!-- Tarjeta 1 -->
    <div class="col-md-4 mb-4">
        <div class="card-container" id="ikigai-card">
            <div class="image-container">
                <?= Html::img('@web/images/pequeIki.png', ['class' => 'imagen-transparente', 'alt' => 'Descripción de la imagen']) ?>
            </div>
            <div class="content-container">
                <h2>Significado de Ikigai</h2>
                <p>Según la cultura japonesa todos tenemos un ikigai, es decir, un propósito de vida o una actividad que nos hace muy felices. Encontrarlo requiere emprender un camino de emprendimiento y crecimiento personal, es decir, un autoconocimiento constante y tenaz que lleva al mejor de los premios: calidad de vida.</p>
            </div>
        </div>
    </div>

    <!-- Tarjeta 2 -->
    <div class="col-md-4 mb-4">
        <div class="card-container" id="Funcionalidad-card">
            <div class="image-container">
                <?= Html::img('@web/images/peqFun.png', ['class' => 'imagen-transparente', 'alt' => 'Descripción de la imagen']) ?>
            </div>
            <div class="content-container">
                <h2>¿Para qué sirve nuestra aplicación?</h2>
                <strong><p>Comprender tus Emociones:</strong> Identifica tus emociones para un mayor autoconocimiento.</p>
                <strong><p>Gestionar el Estrés:</strong> Encuentra técnicas y consejos para manejar el estrés diario.</p>
                <strong><p>Descubrir tu Ikigai:</strong> Explora actividades que te apasionen y contribuyan a tu bienestar general.</p>
            </div>
        </div>
    </div>

    <!-- Tarjeta 3 -->
    <div class="col-md-4 mb-4">
        <div class="card-container" id="Objetivos-card">
            <div class="image-container">
                <?= Html::img('@web/images/peqObj.png', ['class' => 'imagen-transparente', 'alt' => 'Descripción de la imagen']) ?>
            </div>
            <div class="content-container">
                <h2>Objetivos Personales</h2>
                <p>Establecer metas es fundamental para visualizar lo que verdaderamente es importante en tu vida. Te proporciona una dirección clara y te motiva a alcanzar tu máximo potencial. Requiere autodisciplina para superar los obstáculos que inevitablemente surgen en el camino. Al lograr tus metas, experimentas un profundo sentido de logro y satisfacción personal, lo cual fortalece tu autoestima y confianza en ti mismo.</p>
            </div>                  
        </div>
    </div>
</div>
