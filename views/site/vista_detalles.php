<?php

use yii\helpers\Html;
use yii\helpers\Url;
use dosamigos\highcharts\Highcharts;

$this->registerCssFile(Url::to('@web/css/entradasDiario.css'));



?>
   
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
    
<div class="detalles_titulo">
    <h1>Detalles de la Entrada</h1>
</div>

<div class="entry-details">
    <div class="tarjeta_detalles animate__animated animate__fadeInLeft">
        <div class="card-header">
            <h3>Pensamientos:</h3>
        </div>
        <div class="card-body">
          
                <?php foreach ($pensamientos as $pensamiento): ?>
                    <?= Html::encode($pensamiento->texto) ?></li>
                     <p><?php echo ($pensamiento->intrusivo) ? '<strong>Intrusivo: </strong>Si' : '<strong>Intrusivo:</strong> No'; ?></p>
                     <p><?php echo ($pensamiento->recurrente) ? '<strong>Recurrente:</strong> Si' : '<strong>Recurrente:</strong> No'; ?> </p>
                     <p><?php echo ($pensamiento->positivo) ? '<strong>Positivo:</strong> Si' : '<strong>Positivo:</strong> No'; ?></p>
                <?php endforeach; ?>
         
        </div>
    </div>

    <div class="tarjeta_detalles animate__animated animate__fadeInLeft">
        <div class="card-header">
            <h3>Emociones:</h3>
        </div>
        <div class="card-body">
            
                <?php foreach ($emociones as $emocion): ?>
            <p><strong>Intensidad:</strong> <?= Html::encode($emocion->intensidad) ?></p>
                    <p><?= Html::encode($emocion->texto) ?></p>
                    <p><?php echo ($emocion->agradable) ? '<strong>Agradable:</strong> Si' : '<strong>Agradable:</strong> No'; ?></p>
                    <p><strong>Tipos de Emociones: </strong>
                    <?php 
                        $tiposEmociones = $emocion->getTiposemociones()->all(); 
                        $tiposEmocionesString = '';
                        foreach ($tiposEmociones as $index => $tipoEmocion) {
                            $tiposEmocionesString .= Html::encode($tipoEmocion->tipos);
                            //si no es la ultima emoción formateo y pogno la coma en y un espacio
                            if ($index < count($tiposEmociones) - 1) {
                                $tiposEmocionesString .= ', ';
                            }
                        }
                        echo $tiposEmocionesString;
                    ?>
                </p>
  
                <?php endforeach; ?>
        </div>
    </div>

    <div class="tarjeta_detalles animate__animated animate__fadeInLeft">
        <div class="card-header">
            <h3>Sensaciones:</h3>
        </div>
        <div class="card-body">
            
                <?php foreach ($sensaciones as $sensacion): ?>
                <p><strong>Descripción:</strong> <?= Html::encode($sensacion->descripcion) ?></p>
                    <p><strong>Denominación:</strong> <?= Html::encode($sensacion->denominacion) ?></p>
                    <p><strong>Localización Corporal:</strong> <?= Html::encode($sensacion->localizacioncorporal) ?></p>
                <?php endforeach; ?>
          
        </div>
    </div>
</div>


